package com.joelglanfield.moviebite.presenters

import android.view.View
import com.joelglanfield.moviebite.api.RemoteMovieProvider
import com.joelglanfield.moviebite.api.RequestException
import com.joelglanfield.moviebite.coroutines.CoroutineContextProvider
import com.joelglanfield.moviebite.models.GenreResult
import com.joelglanfield.moviebite.models.MovieSearchResult
import com.joelglanfield.moviebite.ui.viewholders.MovieViewHolder
import kotlinx.android.synthetic.main.item_movie.view.*
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.net.UnknownHostException
import kotlin.coroutines.experimental.CoroutineContext

/**
 * This class is the presenter layer for movie search results.
 */
class MovieSearchResultsPresenter(
        private val movieProvider: RemoteMovieProvider, // access to movie requests
        private val coroutineContext: CoroutineContext = CoroutineContextProvider().main // coroutine context (so we can support unit tests)
) : Presenter<MovieSearchResultsPresenter.MovieListView>() {

    companion object {
        // add a slight delay so we're not firing off API requests when a job is about to be cancelled
        private const val SEARCH_DELAY = 500L
    }

    /**
     * The interface to be implemented by the view responsible for displaying movie search results.
     */
    interface MovieListView {
        /**
         * Notifies the view to update the movie results UI.
         * @param movies the updated list of searched movie results
         */
        fun updateMovieListUI(movies: List<MovieSearchResult>)

        /**
         * Notifies the view that a movie item was clicked.
         * @param movie the movie that was clicked
         * @param sharedPosterView the shared view to support an animated transition
         */
        fun movieWasClicked(movie: MovieSearchResult, sharedPosterView: View)

        /**
         * Notifies the view that an error occurred while searching for movies.
         * @param error the request error
         */
        fun showError(error: Exception)
    }

    /**
     * The interface to be implemented by a view holder.
     */
    interface MovieItemView {
        /**
         * Update the view holder's view with the movie details.
         * @param movie update the movie UI with this movie
         * @param genre list of available genres
         */
        fun updateForMovie(movie: MovieSearchResult, genres: List<GenreResult>)
    }

    // the current search query
    private var searchQuery = ""

    // current page of search results
    private var currentPage = 0

    // weather there are more pages to be loaded for the current search query
    var canLoadMore = true

    // the current search Job, if any
    private var searchJob: Job? = null

    // all of the movies that have been fetched from the search results, including new page results
    var movieResults: ArrayList<MovieSearchResult> = arrayListOf()

    /**
     * Search for movies. Here we initialize all search-related properties.
     * @param query the search query
     */
    fun searchMovies(query: String) {
        searchJob?.cancel()

        if (query.isBlank()) {
            return
        }

        searchQuery = query
        currentPage = 0
        movieResults.clear()

        doSearch(searchQuery)
    }

    /**
     * Load more movies, based on the current search query and page.
     */
    fun loadMore() {
        doSearch(searchQuery)
    }

    /**
     * Does the actual search work. Searching is done within a coroutine context. The search
     * job can be cancelled (to handle cases where the user is typing rapidly).
     * @param query the search query
     */
    private fun doSearch(query: String) {
        val searchDelay = if (coroutineContext == Unconfined) 0L else SEARCH_DELAY
        searchJob = launch(coroutineContext) {
            try {
                delay(searchDelay)

                // only search if the job wasn't cancelled
                if (isActive) {
                    val movieListResult = movieProvider.searchMovies(query, currentPage + 1)

                    // movieListResult will be null when unit testing
                    if (movieListResult != null) {
                        currentPage = movieListResult.page
                        canLoadMore = currentPage < movieListResult.totalPages
                        movieResults.addAll(movieListResult.results)
                    }
                }

                // only display if the job wasn't cancelled
                if (isActive) {
                    view?.updateMovieListUI(movieResults)
                }

                searchJob = null
            } catch (e: Exception) {
                if (e is UnknownHostException || e is RequestException) {
                    view?.showError(e)
                }
            }
        }
    }

    /**
     * Tells the movie view to bind itself to the movie.
     * @param position the position of the movie to bind
     * @param movieView the movie view
     */
    fun onBindMovieItemView(position: Int, movieView: MovieItemView) {
        if (position < movieResults.size) {
            val movie = movieResults[position]
            movieView.updateForMovie(movie, movieProvider.genresList)
        }
    }

    /**
     * Notifies the movie list view that a movie was clicked.
     * @param position the position of the clicked movie
     */
    fun onMovieClicked(position: Int, movieViewHolder: MovieViewHolder) {
        if (position < movieResults.size) {
            view?.movieWasClicked(movieResults[position], movieViewHolder.itemView.moviePosterImageView)
        }
    }

}