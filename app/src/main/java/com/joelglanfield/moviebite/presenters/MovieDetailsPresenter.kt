package com.joelglanfield.moviebite.presenters

import com.joelglanfield.moviebite.api.RemoteMovieProvider
import com.joelglanfield.moviebite.coroutines.CoroutineContextProvider
import com.joelglanfield.moviebite.models.MovieDetailsResult
import kotlinx.coroutines.experimental.launch
import kotlin.coroutines.experimental.CoroutineContext

/**
 * This class is the presenter layer for movie details.
 */
class MovieDetailsPresenter(
        private val movieProvider: RemoteMovieProvider, // access to movie requests
        private val coroutineContext: CoroutineContext = CoroutineContextProvider().main // coroutine context (so we can support unit tests)
) : Presenter<MovieDetailsPresenter.MovieDetailsView>() {

    /**
     * The interface to be implemented by the view responsible for displaying movie details.
     */
    interface MovieDetailsView {
        /**
         * Notifies the view to display movie details.
         * @param movieDetails the movie details to display
         */
        fun displayMovieDetails(movieDetails: MovieDetailsResult?)

        /**
         * Notifies the view that an error occurred while searching for movies.
         * @param error the request error
         */
        fun showError(error: Exception)
    }

    /**
     * Fetch details for the movie with the specified id.
     * @param movieId the id of the movie whose details we are requesting
     */
    fun fetchMovieDetails(movieId: Long) {
        if (movieId < 0) {
            return
        }

        launch(coroutineContext) {
            try {
                val movieDetails = movieProvider.requestMovieDetails(movieId)
                view?.displayMovieDetails(movieDetails)
            } catch (e: Exception) {
                view?.showError(e)
            }
        }
    }
}