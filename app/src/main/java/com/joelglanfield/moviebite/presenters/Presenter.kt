package com.joelglanfield.moviebite.presenters

abstract class Presenter<V> {
    protected var view: V? = null

    /**
     * Bind the view.
     * @param view the view to be bound to the presenter
     */
    fun bind(view: V) {
        this.view = view
    }

    /**
     * Unbind the view from the presenter.
     */
    fun unbind() {
        this.view = null
    }
}