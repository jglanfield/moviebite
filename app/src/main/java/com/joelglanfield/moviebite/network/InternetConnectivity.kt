package com.joelglanfield.moviebite.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Handler

enum class InternetState {
    AVAILABLE, UNAVAILABLE
}

interface InternetConnectivityListener {
    /**
     * Notifies the listener that the internet state has changed.
     * @param state whether available or unavailable
     */
    fun internetStateChanged(state: InternetState)
}

object InternetConnectivityMonitor : ConnectivityManager.NetworkCallback() {

    private val request by lazy {
        NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build()
    }

    // the connectivity manager
    private var connectivityManager: ConnectivityManager? = null

    // current internet availability
    var currentInternetState = InternetState.AVAILABLE

    // listener to receive internet state updates
    var listener: InternetConnectivityListener? = null

    /**
     * Begin monitoring the internet state.
     * @param context Context
     */
    fun beginMonitoring(context: Context) {
        if (connectivityManager == null) {
            connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        }

        connectivityManager?.registerNetworkCallback(request, this)
        updateInternetState()
    }

    /**
     * Stop monitoring the internet state.
     */
    fun stopMonitoring() {
        connectivityManager?.unregisterNetworkCallback(this)
    }

    /**
     * Called when the internet becomes available.
     * @param network Network
     */
    override fun onAvailable(network: Network) {
        Handler().postDelayed({
            updateInternetState()
        }, 1000L)
    }

    /**
     * Called when the internet becomes unavailable.
     * @param network Network
     */
    override fun onLost(network: Network) {
        Handler().postDelayed({
            updateInternetState()
        }, 1000L)
    }

    /**
     * Updates the internet state and notifies the listener, if the state has indeed changed.
     */
    private fun updateInternetState() {
        val connected = hasInternetConnection()

        val previousState = currentInternetState
        currentInternetState = if (connected) InternetState.AVAILABLE else InternetState.UNAVAILABLE

        if (previousState != currentInternetState) {
            listener?.internetStateChanged(currentInternetState)
        }
    }

    /**
     * Gets whether we have an active internet connection.
     * @return whether we have an active internet connection
     */
    private fun hasInternetConnection(): Boolean {
        try {
            if (connectivityManager?.activeNetworkInfo?.type == ConnectivityManager.TYPE_WIFI ||
                            connectivityManager?.activeNetworkInfo?.type == ConnectivityManager.TYPE_MOBILE) {
                return true
            }
        } catch (e: IllegalStateException) {
            return false
        }
        return false
    }
}