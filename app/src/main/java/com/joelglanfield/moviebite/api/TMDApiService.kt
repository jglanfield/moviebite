package com.joelglanfield.moviebite.api

import com.joelglanfield.moviebite.models.GenreListResult
import com.joelglanfield.moviebite.models.MovieDetailsResult
import com.joelglanfield.moviebite.models.MovieListResult
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * The Retrofit Service for The Movie Database APIs we wish to use in the app.
 */
interface TMDApiService {
    /**
     * Search movies provided some query text.
     * @param apiKey the api key
     * @param query the query text
     * @param page the results page
     * @param adultContent whether to include adult content
     */
    @GET("search/movie")
    fun search(@Query("api_key") apiKey: String,
               @Query("query") query: String,
               @Query("page") page: Int,
               @Query("include_adult") adultContent: Boolean = false): Call<MovieListResult>

    /**
     * Request the list of genres for all movies.
     * @param apiKey the api key
     */
    @GET("genre/movie/list")
    fun requestGenres(@Query("api_key") apiKey: String): Call<GenreListResult>

    /**
     * Request the details for a movie.
     * @param movieId the id of the movie to get details for
     * @param apiKey the api key
     */
    @GET("movie/{movie_id}")
    fun requestMovieDetails(@Path("movie_id") movieId: Long,
                            @Query("api_key") apiKey: String): Call<MovieDetailsResult>

    companion object {
        private const val BASE_URL = "https://api.themoviedb.org/3/"

        /**
         * Creates a movie api service.
         * @return MovieApiService
         */
        fun create(): TMDApiService {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC

            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build()

            return retrofit.create(TMDApiService::class.java)
        }
    }
}