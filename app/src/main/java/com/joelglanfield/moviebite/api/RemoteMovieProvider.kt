package com.joelglanfield.moviebite.api

import android.util.Log
import com.joelglanfield.moviebite.coroutines.CoroutineContextProvider
import com.joelglanfield.moviebite.extensions.suspendEnqueue
import com.joelglanfield.moviebite.models.GenreListResult
import com.joelglanfield.moviebite.models.GenreResult
import com.joelglanfield.moviebite.models.MovieDetailsResult
import com.joelglanfield.moviebite.models.MovieListResult
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.run
import kotlin.coroutines.experimental.CoroutineContext

/**
 * Provides the required API calls to The Movie Database. For the purpose of this application,
 * we are (currently) only concerned with searching movies and fetching genres.
 */
class RemoteMovieProvider(
        private val coroutineContext: CoroutineContext = CoroutineContextProvider().main // coroutine context (so we can support unit tests)
) {
    companion object {
        private const val TAG = "RemoteMovieProvider"
        private const val API_KEY_VALUE = "4cf4f92e35bdd256c66f3c09a80fae58"

        // list routes as needed
        const val IMAGE_URL = "https://image.tmdb.org/t/p/w92"
        const val BACKDROP_IMAGE_URL = "https://image.tmdb.org/t/p/w500"
    }

    // the service that specifies the Retrofit queries
    private val apiService: TMDApiService = TMDApiService.create()

    // keep a list of all genres, since there aren't many
    var genresList: List<GenreResult> = emptyList()

    init {
        launch(coroutineContext) {
            try {
                genresList = requestGenres().genres
            } catch (e: Exception) {
                Log.e(TAG, "Couldn't fetch genres")
            }
        }
    }

    /**
     * Searches the movie database for all movies that satisfy a given query. Results are paginated.
     * @param query the search query
     * @param page the page of results
     */
    suspend fun searchMovies(query: String, page: Int): MovieListResult = run(coroutineContext) {
        try {
            // spaces in movie titles should be replaced with "+"
            val encodedQuery = query.replace(" ", "+")
            apiService.search(API_KEY_VALUE, encodedQuery, page).suspendEnqueue()
        } catch (e: Exception) {
            throw e
        }
    }

    /**
     * Fetches all available genres. Movies fetched in a search will contain a list of genre IDs
     * that will be matched against the results fetched here.
     */
    suspend fun requestGenres(): GenreListResult = run(coroutineContext) {
        try {
            apiService.requestGenres(API_KEY_VALUE).suspendEnqueue()
        } catch (e: Exception) {
            throw e
        }
    }

    /**
     * Requests the details for the movie with the specified id.
     * @param movieId the id of the movie
     */
    suspend fun requestMovieDetails(movieId: Long): MovieDetailsResult = run(coroutineContext) {
        try {
            apiService.requestMovieDetails(movieId, API_KEY_VALUE).suspendEnqueue()
        } catch (e: Exception) {
            throw e
        }
    }
}

/** Convenience class for custom network request exceptions. */
class RequestException(val reason: String, val code: Int? = -1): RuntimeException(reason)

/** Allows us to create {@code RequestExceptions} for reasons listed only in this enum. */
enum class RequestError(private val reason: String) {
    MISSING_RESPONSE_BODY("Server error: missing response body"),
    UNKNOWN_SERVER_ERROR("Server error: unknown");

    fun toException(): Exception {
        return RequestException(reason)
    }
}
