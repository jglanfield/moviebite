package com.joelglanfield.moviebite.ui.viewholders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.joelglanfield.moviebite.R
import com.joelglanfield.moviebite.api.RemoteMovieProvider
import com.joelglanfield.moviebite.extensions.getYear
import com.joelglanfield.moviebite.models.GenreResult
import com.joelglanfield.moviebite.models.MovieSearchResult
import com.joelglanfield.moviebite.presenters.MovieSearchResultsPresenter
import java.text.SimpleDateFormat
import java.util.*

/**
 * View holder for movie views.
 */
class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), MovieSearchResultsPresenter.MovieItemView {

    // displays the title of the movie
    private val titleTextView: TextView = itemView.findViewById(R.id.movieTitleTextView)

    // displays the rating of the movie
    private val ratingTextView: TextView = itemView.findViewById(R.id.movieRatingTextView)

    // displays the release date of the movie
    private val releaseDateTextView: TextView = itemView.findViewById(R.id.movieReleaseDateTextView)

    // displays the genre(s) of the movie
    private val genresTextView: TextView = itemView.findViewById(R.id.movieGenresTextView)

    // displays the popularity of the movie as an icon
    private val popularityImageView: ImageView = itemView.findViewById(R.id.moviePopularityImageView)

    // displays the poster for the movie
    private val posterImageView: ImageView = itemView.findViewById(R.id.moviePosterImageView)

    /**
     * Update the view holder's view with the supplied movie data.
     * @param movie the movie to display
     * @param genres the genres to search
     */
    override fun updateForMovie(movie: MovieSearchResult, genres: List<GenreResult>) {
        titleTextView.text = movie.title
        ratingTextView.text = movie.voteAverage.toString()

        // release date -> extract the year
        releaseDateTextView.text = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).getYear(movie.releaseDate)

        // popularity icon
        if (movie.popularity >= movie.voteAverage) {
            popularityImageView.setImageResource(R.drawable.ic_popularity_up)
        } else {
            popularityImageView.setImageResource(R.drawable.ic_popularity_down)
        }

        // poster icon
        posterImageView.clipToOutline = true
        posterImageView.transitionName = movie.id.toString()
        if (movie.posterPath.isNullOrBlank()) {
            posterImageView.setImageResource(R.drawable.ic_missing_poster)
        } else {
            Glide.with(itemView)
                    .load(RemoteMovieProvider.IMAGE_URL + movie.posterPath)
                    .into(posterImageView)
        }

        // genres
        val genreResults = genres.filter { movie.genreIds.contains(it.id) }.map { it.name }
        genresTextView.text = genreResults.joinToString(", ")
    }
}