package com.joelglanfield.moviebite.ui.adapters

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.joelglanfield.moviebite.R
import com.joelglanfield.moviebite.models.MovieSearchResult
import com.joelglanfield.moviebite.presenters.MovieSearchResultsPresenter
import com.joelglanfield.moviebite.ui.viewholders.MovieViewHolder

/**
 * Custom list adapter for our movies recycler view.
 */
class MovieSearchResultsAdapter(private val presenter: MovieSearchResultsPresenter) : ListAdapter<MovieSearchResult, MovieViewHolder>(MovieDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder =
            MovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false))

    /**
     * Binds the movie view holder to the movie at the given position.
     * @param holder the ViewHolder
     * @param position the position of the view holder (used to retrieve the movie)
     */
    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        presenter.onBindMovieItemView(position, holder)
        holder.itemView.setOnClickListener {
            presenter.onMovieClicked(position, holder)
        }
    }
}

/**
 * Allows the ListAdapter to conduct a diff when we submit a new list of movies.
 */
class MovieDiffCallback : DiffUtil.ItemCallback<MovieSearchResult>() {
    override fun areItemsTheSame(oldItem: MovieSearchResult?, newItem: MovieSearchResult?): Boolean {
        return oldItem?.id == newItem?.id
    }

    override fun areContentsTheSame(oldItem: MovieSearchResult?, newItem: MovieSearchResult?): Boolean {
        return oldItem == newItem
    }
}