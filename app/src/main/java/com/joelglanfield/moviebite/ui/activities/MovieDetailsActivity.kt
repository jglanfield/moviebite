package com.joelglanfield.moviebite.ui.activities

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.AccelerateInterpolator
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.joelglanfield.moviebite.R
import com.joelglanfield.moviebite.api.RemoteMovieProvider
import kotlinx.android.synthetic.main.activity_movie_details.*
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.joelglanfield.moviebite.api.RequestException
import com.joelglanfield.moviebite.extensions.getYear
import com.joelglanfield.moviebite.models.MovieDetailsResult
import com.joelglanfield.moviebite.presenters.MovieDetailsPresenter
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*

/**
 * The activity that is responsible for displaying movie details.
 */
class MovieDetailsActivity : AppCompatActivity(), MovieDetailsPresenter.MovieDetailsView {

    companion object {
        // key for a bundled movie id
        const val movieIdExtraKey = "movieIdExtra"

        // key for the poster transition name
        const val moviePosterTransitionNameKey = "moviePosterTransitionName"
    }

    // the movie details presenter
    private lateinit var movieDetailsPresenter: MovieDetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        movieDetailsPresenter = MovieDetailsPresenter(RemoteMovieProvider())
        movieDetailsPresenter.bind(this)

        val movieId = intent.getLongExtra(movieIdExtraKey, -1)
        movieDetailsPresenter.fetchMovieDetails(movieId)

        // we need to postpone the enter transition to give Glide time to load the poster image
        if (intent.extras.containsKey(moviePosterTransitionNameKey)) {
            movieDetailsPosterImageView.transitionName = intent.extras.getString(moviePosterTransitionNameKey)
            supportPostponeEnterTransition()
        }

        movieDetailsBackButton.setOnClickListener {
            finish()
        }
    }

    /**
     * Perform necessary cleanup.
     */
    override fun onDestroy() {
        super.onDestroy()

        movieDetailsPresenter.unbind()
    }

    /**
     * Displays the movie results, including: title, overview, poster, backgrop, year, and genres.
     * @param movieDetails the details of the movie we want to display
     */
    override fun displayMovieDetails(movieDetails: MovieDetailsResult?) {
        // need to support optional param for unit tests
        if (movieDetails == null) {
            return
        }

        val backdropOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_missing_poster)

        movieDetails.backdropPath?.let {
            Glide.with(this)
                    .load(RemoteMovieProvider.BACKDROP_IMAGE_URL + it)
                    .apply(backdropOptions)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            revealBackdrop()
                            return false
                        }
                    })
                    .into(movieDetailsBackdropImageView)

        }

        if (movieDetails.backdropPath.isNullOrBlank()) {
            revealBackdrop()
        }

        val posterOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_missing_poster)
                .dontTransform()
                .onlyRetrieveFromCache(true)

        movieDetailsPosterImageView.clipToOutline = true
        movieDetails.posterPath?.let {
            Glide.with(this)
                    .load(RemoteMovieProvider.IMAGE_URL + it)
                    .apply(posterOptions)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            supportStartPostponedEnterTransition()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            supportStartPostponedEnterTransition()
                            return false
                        }
                    })
                    .into(movieDetailsPosterImageView)
        }

        if (movieDetails.posterPath.isNullOrBlank()) {
            supportStartPostponedEnterTransition()
        }

        movieDetailsOverviewTextView.text = movieDetails.overview ?: "Missing overview"
        movieDetailsTitleTextView.text = movieDetails.title
        movieDetailsGenresTextView.text = movieDetails.genres.joinToString(", ") { it.name }
        movieDetailsReleaseDateTextView.text = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).getYear(movieDetails.releaseDate)
    }

    /**
     * Show an error.
     * @param error the error to create the Snackbar message from
     */
    override fun showError(error: Exception) {
        val message = when (error) {
            is RequestException -> "${error.reason} (${error.code})"
            is UnknownHostException -> getString(R.string.search_network_error)
            else -> error.localizedMessage
        }

        supportStartPostponedEnterTransition()
        Snackbar.make(movieDetailsBackdropImageView, message, Snackbar.LENGTH_LONG).show()
    }

    /**
     * Reveal the movie backgrop image using a circular reveal ala the Google Play Store.
     */
    private fun revealBackdrop() {
        val cx = movieDetailsBackdropCoverView.width / 2
        val cy = movieDetailsBackdropCoverView.height - 100
        val startRadius = Math.hypot(cx.toDouble(), cy.toDouble())

        val animation = ViewAnimationUtils.createCircularReveal(movieDetailsBackdropCoverView, cx, cy, startRadius.toFloat(), 0F)
        animation.duration = 500L
        animation.interpolator = AccelerateInterpolator()
        animation.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                movieDetailsBackdropCoverView.visibility = View.GONE
            }
        })

        animation.start()
    }
}