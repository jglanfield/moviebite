package com.joelglanfield.moviebite.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.joelglanfield.moviebite.R
import com.joelglanfield.moviebite.api.RemoteMovieProvider
import com.joelglanfield.moviebite.api.RequestException
import com.joelglanfield.moviebite.models.MovieSearchResult
import com.joelglanfield.moviebite.network.InternetConnectivityListener
import com.joelglanfield.moviebite.network.InternetConnectivityMonitor
import com.joelglanfield.moviebite.network.InternetState
import com.joelglanfield.moviebite.presenters.MovieSearchResultsPresenter
import com.joelglanfield.moviebite.ui.activities.MovieDetailsActivity.Companion.movieIdExtraKey
import com.joelglanfield.moviebite.ui.activities.MovieDetailsActivity.Companion.moviePosterTransitionNameKey
import com.joelglanfield.moviebite.ui.adapters.MovieSearchResultsAdapter
import kotlinx.android.synthetic.main.activity_tmd_search.*
import java.net.UnknownHostException
import java.util.*

/**
 * The activity that is responsible for search input and displaying search results.
 */
class TMDSearchActivity : AppCompatActivity(), MovieSearchResultsPresenter.MovieListView, InternetConnectivityListener {

    // list adapter for our movies recycler view
    private lateinit var movieResultsAdapter: MovieSearchResultsAdapter

    // movies presenter layer
    val moviesPresenter: MovieSearchResultsPresenter by lazy { MovieSearchResultsPresenter(RemoteMovieProvider()) }

    // number of movies currently being displayed
    private var moviesCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tmd_search)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        moviesPresenter.bind(this)

        movieResultsAdapter = MovieSearchResultsAdapter(moviesPresenter)

        configureViews()
    }

    override fun onResume() {
        super.onResume()

        InternetConnectivityMonitor.listener = this
        InternetConnectivityMonitor.beginMonitoring(this)

        if (InternetConnectivityMonitor.currentInternetState == InternetState.UNAVAILABLE) {
            handleInternetState(InternetState.UNAVAILABLE)
        }
    }

    override fun onPause() {
        super.onPause()

        InternetConnectivityMonitor.listener = null
        InternetConnectivityMonitor.stopMonitoring()
    }

    /**
     * Perform necessary cleanup.
     */
    override fun onDestroy() {
        super.onDestroy()

        moviesPresenter.unbind()
        moviesRecyclerView.adapter = null
    }

    /**
     * Configure the necessary views, adding listeners as needed.
     */
    private fun configureViews() {
        moviesRecyclerView.layoutManager = LinearLayoutManager(this)
        moviesRecyclerView.adapter = movieResultsAdapter
        moviesRecyclerView.setHasFixedSize(true)

        searchImageButton.setOnClickListener { showSearchUI(true) }
        searchBackButton.setOnClickListener { showSearchUI(false) }

        searchEditText.setOnFocusChangeListener { _, hasFocus ->
            searchImageButton.visibility = if (hasFocus) View.INVISIBLE else View.VISIBLE
            searchBackButton.visibility = if (hasFocus) View.VISIBLE else View.INVISIBLE
        }

        searchEditText.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
                showLoading(text?.isNotBlank() == true)
                moviesPresenter.searchMovies(text.toString())

                if (text?.isBlank() == true) {
                    movieResultsAdapter.submitList(emptyList())
                    moviesCTALayout.visibility = View.VISIBLE
                    moviesNoResultsTextView.visibility = View.INVISIBLE
                }
            }

            override fun afterTextChanged(text: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        })

        // use a scroll listener to determine whether we should attempt to load more results
        moviesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy <= 0 || moviesCount == 0 || !moviesPresenter.canLoadMore) {
                    return
                }

                (recyclerView.layoutManager as? LinearLayoutManager)?.let {
                    if (loadingContainer.visibility != View.VISIBLE && it.findLastVisibleItemPosition() >= moviesCount - 3) {
                        showLoading(true)
                        moviesPresenter.loadMore()
                    }
                }
            }
        })
    }

    /**
     * If the search EditText has focus, then pressing the back button should clear its focus.
     * Otherwise, the back button behaves as expected.
     * @param keyCode
     * @param KeyEvent?
     */
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && searchEditText.isFocused) {
            searchEditText.clearFocus()
            return false
        }

        return super.onKeyDown(keyCode, event)
    }

    /**
     * Update the movie list UI with the search results.
     * @param movies the search results
     */
    override fun updateMovieListUI(movies: List<MovieSearchResult>) {
        moviesCount = movies.size

        // new list, so the list adapter knows to do the diff
        val allMovies: ArrayList<MovieSearchResult> = arrayListOf()
        allMovies.addAll(movies)
        movieResultsAdapter.submitList(allMovies)

        showLoading(false)
        moviesCTALayout.visibility = View.INVISIBLE
        moviesNoResultsTextView.visibility = if (movies.isEmpty()) View.VISIBLE else View.INVISIBLE
    }

    /**
     * When a movie is tapped, show the movie details Activity.
     * @param movie the tapped movie
     * @param view view for shared element transition
     */
    override fun movieWasClicked(movie: MovieSearchResult, sharedPosterView: View) {
        val movieDetailsIntent = Intent(this, MovieDetailsActivity::class.java)
        movieDetailsIntent.putExtra(movieIdExtraKey, movie.id)
        movieDetailsIntent.putExtra(moviePosterTransitionNameKey, ViewCompat.getTransitionName(sharedPosterView))

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, sharedPosterView, ViewCompat.getTransitionName(sharedPosterView))
        startActivity(movieDetailsIntent, options.toBundle())
    }

    /**
     * Handles internet state changes.
     * @param state the current internet state
     */
    override fun internetStateChanged(state: InternetState) {
        handleInternetState(state)
    }

    /**
     * When the internet state changes, update the CTA and error states to inform the user.
     * @param state the current internet state
     */
    private fun handleInternetState(state: InternetState) {
        runOnUiThread {
            if (moviesCount == 0) {
                moviesNoInternetTextView.visibility = if (state == InternetState.AVAILABLE) View.INVISIBLE else View.VISIBLE
                moviesCTALayout.visibility = if (state == InternetState.AVAILABLE) View.VISIBLE else View.INVISIBLE
            }
        }
    }

    /**
     * Toggles the search UI elements. If the user is to begin searching,
     * then we hide the search icon, present the back (or up) icon, give focus
     * to the text input, and present the keyboard.
     * @param show whether to show search UI (meaning the search EditText receives focus)
     */
    private fun showSearchUI(show: Boolean) {
        if (show) {
            searchEditText.requestFocus()
        } else {
            searchEditText.clearFocus()
        }

        showKeyboard(show)
    }

    /**
     * Convenience method for toggling the keyboard.
     * @param show whether to show the keyboard
     */
    private fun showKeyboard(show: Boolean) {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        if (show) {
            inputManager.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT)
        } else {
            inputManager.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

    /**
     * Whether to display the loading indicator.
     * @param show whether to show the loading indicator
     */
    private fun showLoading(show: Boolean) {
        loadingContainer.visibility = if (show) View.VISIBLE else View.GONE
    }

    /**
     * Show an error.
     * @param error the error to create the Snackbar message from
     */
    override fun showError(error: Exception) {
        val message = when (error) {
            is RequestException -> "${error.reason} (${error.code})"
            is UnknownHostException -> getString(R.string.search_network_error)
            else -> error.localizedMessage
        }
        Snackbar.make(moviesRecyclerView, message, Snackbar.LENGTH_LONG).show()

        showLoading(false)
    }
}

