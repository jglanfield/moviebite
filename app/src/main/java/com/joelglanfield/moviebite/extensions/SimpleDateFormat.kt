package com.joelglanfield.moviebite.extensions

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Gets the year from the supplied date string.
 * @param date the date to parse
 * @return the year as a String. The empty string is returned when no year is found.
 */
fun SimpleDateFormat.getYear(date: String): String {
    return try {
        val parsedDate = parse(date)
        val calendar = Calendar.getInstance()
        calendar.time = parsedDate

        calendar.get(Calendar.YEAR).toString()
    } catch (e: ParseException) {
        ""
    }
}