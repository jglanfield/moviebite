package com.joelglanfield.moviebite.extensions

import com.joelglanfield.moviebite.api.RequestError
import com.joelglanfield.moviebite.api.RequestException
import kotlinx.coroutines.experimental.CompletableDeferred
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Extends retrofit2.Call to support calling from coroutines.
 * @throws RequestException if the network request fails.
 */
suspend fun <T> Call<T>.suspendEnqueue(): T {
    val deferred = CompletableDeferred<T>()

    enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>?, response: Response<T>?) {
            val body = response?.body()
            if (body != null) {
                if (!response.isSuccessful) {
                    deferred.completeExceptionally(RequestException(response.message(), response.code()))
                } else {
                    deferred.complete(body)
                }
            } else {
                deferred.completeExceptionally(RequestError.MISSING_RESPONSE_BODY.toException())
            }
        }

        override fun onFailure(call: Call<T>?, t: Throwable?) {
            deferred.completeExceptionally(t ?: RequestError.UNKNOWN_SERVER_ERROR.toException())
        }
    })

    return deferred.await()
}