package com.joelglanfield.moviebite.coroutines

import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.android.UI
import kotlin.coroutines.experimental.CoroutineContext

/**
 * We need coroutine context providers so we can run unit tests on functions that employ coroutines.
 * @code {CoroutineContextProvider} simply provides the UI context.
 * @code {TestContextProvider} provides the Unconfined context for unit testing.
 */


open class CoroutineContextProvider {
    open val main: CoroutineContext by lazy { UI }
}

class TestContextProvider : CoroutineContextProvider() {
    override val main: CoroutineContext = Unconfined
}