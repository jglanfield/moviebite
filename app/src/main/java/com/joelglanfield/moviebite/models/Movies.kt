package com.joelglanfield.moviebite.models

import com.squareup.moshi.Json

/**
 * A collection of movies as a result of a search.
 * @param page the current page of search results
 * @param totalPages total number of available pages of search results
 * @param results the actual movie search results
 */
data class MovieListResult(val page: Int,
                           @Json(name = "total_pages") val totalPages: Int,
                           @Json(name = "total_results") val totalResults: Int,
                           val results: List<MovieSearchResult> = emptyList())

/**
 * A movie that is part of a list of search results.
 * @param id the id of the movie
 * @param title the title of the movie
 * @param posterPath the path of the movie's poster
 * @param popularity current popularity of the movie
 * @param voteAverage the average vote for the movie
 * @param releaseDate the release date of the movie
 * @param genreIds the ids of all genres for the movie
 */
data class MovieSearchResult(val id: Long,
                             val title: String,
                             @Json(name = "poster_path") val posterPath: String?,
                             val popularity: Double,
                             @Json(name = "vote_average") val voteAverage: Double,
                             @Json(name = "release_date") val releaseDate: String,
                             @Json(name = "genre_ids") val genreIds: List<Int> = emptyList())

/**
 * The details for a movie.
 * @param id the id of the movie
 * @param title the title of the movie
 * @param posterPath the path of the movie's poster
 * @param backdropPath the path of the movie's backdrop
 * @param overview the overview of the movie
 * @param popularity current popularity of the movie
 * @param voteAverage the average vote for the movie
 * @param releaseDate the release date of the movie
 * @param genres all genres associated with this movie
 */
data class MovieDetailsResult(val id: Long,
                             val title: String,
                             @Json(name = "poster_path") val posterPath: String?,
                             @Json(name = "backdrop_path") val backdropPath: String?,
                             val overview: String?,
                             val popularity: Double,
                             @Json(name = "vote_average") val voteAverage: Double,
                             @Json(name = "release_date") val releaseDate: String,
                             @Json(name = "genres") val genres: List<GenreResult> = emptyList())