package com.joelglanfield.moviebite.models

/**
 * A list of genres.
 * @param genres the list of GenreResult instances
 */
data class GenreListResult(val genres: List<GenreResult> = emptyList())

/**
 * A genre.
 * @param id the id of the genre
 * @param name the name of the genre
 */
data class GenreResult(val id: Int, val name: String)