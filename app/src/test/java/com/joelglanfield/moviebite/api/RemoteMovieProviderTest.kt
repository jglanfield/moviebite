package com.joelglanfield.moviebite.api

import com.joelglanfield.moviebite.models.*
import com.nhaarman.mockito_kotlin.*
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Assert.*
import org.junit.Test

class RemoteMovieProviderTest {

    // remote movie provider
    private val movieProvider: RemoteMovieProvider = mock()

    @Test
    fun requestGenres_nonEmptyResults() {
        val movieProvider: RemoteMovieProvider = mock()

        val genreResult = GenreResult(0, "Joel")
        val genreListResult = GenreListResult(listOf(genreResult))


        runBlocking {
            doReturn(genreListResult).`when`(movieProvider).requestGenres()

            val results = movieProvider.requestGenres()
            assert(results.genres.isNotEmpty())
            assertEquals(results.genres.first().id, 0)
            assertEquals(results.genres.first().name, "Joel")
        }
    }

    @Test
    fun requestGenres_emptyResults() {
        val genreListResult = GenreListResult(emptyList())

        runBlocking {
            doReturn(genreListResult).`when`(movieProvider).requestGenres()

            val results = movieProvider.requestGenres()
            assert(results.genres.isEmpty())
        }
    }

    @Test
    fun requestGenres_exception() {
        val requestException = RequestError.MISSING_RESPONSE_BODY.toException()

        runBlocking {
            try {
                doThrow(requestException).`when`(movieProvider).requestGenres()
                movieProvider.requestGenres()
            } catch (e: Exception) {
                assert(e is RequestException)
            }
        }
    }

    @Test
    fun searchMovies_nonEmptyResults() {
        val movieSearchResult = MovieSearchResult(0, "title", "poster_path", 0.0, 0.0, "release_date", emptyList())
        val movieListResult = MovieListResult(0, 0, 0, listOf(movieSearchResult))

        runBlocking {
            doReturn(movieListResult).`when`(movieProvider).searchMovies("s", 1)

            val movies = movieProvider.searchMovies("s", 1)
            assert(movies.results.isNotEmpty())

            val movie = movies.results.first()
            assertEquals(movie.id, 0)
            assertEquals(movie.title, "title")
            assertEquals(movie.posterPath, "poster_path")
            assertEquals(movie.popularity, 0.0, 0.01)
            assertEquals(movie.voteAverage, 0.0, 0.01)
            assertEquals(movie.releaseDate, "release_date")
            assert(movie.genreIds.isEmpty())
        }
    }

    @Test
    fun searchMovies_emptyResults() {
        val movieListResult = MovieListResult(0, 0, 0, emptyList())

        runBlocking {
            doReturn(movieListResult).`when`(movieProvider).searchMovies("s", 1)

            val movies = movieProvider.searchMovies("s", 1)
            assert(movies.results.isEmpty())
        }
    }

    @Test
    fun searchMovies_exception() {
        val requestException = RequestError.MISSING_RESPONSE_BODY.toException()

        runBlocking {
            try {
                doThrow(requestException).`when`(movieProvider).searchMovies("s", 0)
                movieProvider.searchMovies("s", 0)
            } catch (e: Exception) {
                assert(e is RequestException)
            }
        }
    }

    @Test
    fun requestMovieDetails_nonEmptyResult() {
        val movieDetailsResult = MovieDetailsResult(0, "title", "poster_path", "backdrop_path", "overview", 0.0, 0.0, "release_date", emptyList())

        runBlocking {
            doReturn(movieDetailsResult).`when`(movieProvider).requestMovieDetails(0)

            val movieDetails = movieProvider.requestMovieDetails(0)
            assertEquals(movieDetails.id, 0)
            assertEquals(movieDetails.title, "title")
            assertEquals(movieDetails.posterPath, "poster_path")
            assertEquals(movieDetails.backdropPath, "backdrop_path")
            assertEquals(movieDetails.overview, "overview")
            assertEquals(movieDetails.popularity, 0.0, 0.01)
            assertEquals(movieDetails.voteAverage, 0.0, 0.01)
            assertEquals(movieDetails.releaseDate, "release_date")
            assert(movieDetails.genres.isEmpty())
        }
    }

    @Test
    fun requestMovieDetails_exception() {
        val requestException = RequestError.MISSING_RESPONSE_BODY.toException()

        runBlocking {
            try {
                doThrow(requestException).`when`(movieProvider).requestMovieDetails(0)
                movieProvider.requestMovieDetails(0)
            } catch (e: Exception) {
                assert(e is RequestException)
            }
        }
    }
}