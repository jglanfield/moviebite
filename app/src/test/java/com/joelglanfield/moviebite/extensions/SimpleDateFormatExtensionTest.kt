package com.joelglanfield.moviebite.extensions

import org.junit.Assert.*
import org.junit.Test
import java.text.SimpleDateFormat


class SimpleDateFormatExtensionTest {

    @Test
    fun getYear_emptyDate() {
        val date = ""
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")

        val year = dateFormat.getYear(date)
        assertEquals(year, "")
    }

    @Test
    fun getYear_invalidDate() {
        val date = "2007-A-B"
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")

        val year = dateFormat.getYear(date)
        assertEquals(year, "")
    }

    @Test
    fun getYear_validDate() {
        val date = "2018-10-11"
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")

        val year = dateFormat.getYear(date)
        assertEquals(year, "2018")
    }
}