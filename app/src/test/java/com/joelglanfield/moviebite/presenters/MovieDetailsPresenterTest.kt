package com.joelglanfield.moviebite.presenters

import com.joelglanfield.moviebite.api.RemoteMovieProvider
import com.joelglanfield.moviebite.coroutines.TestContextProvider
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Test

class MovieDetailsPresenterTest {
    // mocks
    private val movieProvider: RemoteMovieProvider = mock()
    private val view: MovieDetailsPresenter.MovieDetailsView = mock()

    // the movie details presenter
    private lateinit var presenter: MovieDetailsPresenter

    @Before
    fun setup() {
        presenter = MovieDetailsPresenter(movieProvider, TestContextProvider().main)
    }

    /**
     * When requesting movie details on an invalid movie id,
     * RemoteMovieProvider.requestMovieDetails() should never be called.
     */
    @Test
    fun fetchMovieDetails_invalidMovieId_neverCallsRequestMovieDetails() {
        val movieDetails = null

        presenter.bind(view)

        runBlocking {
            presenter.fetchMovieDetails(-1L)
            verify(view, never()).displayMovieDetails(movieDetails)
        }
    }

    /**
     * When requesting movie details on a valid movie id,
     * RemoteMovieProvider.requestMovieDetails() should be called precisely once.
     */
    @Test
    fun fetchMovieDetails_validMovieId_callsRequestMovieDetails() {
        val movieDetails = null

        presenter.bind(view)

        runBlocking {
            presenter.fetchMovieDetails(1L)
            verify(view, times(1)).displayMovieDetails(movieDetails)
        }
    }
}