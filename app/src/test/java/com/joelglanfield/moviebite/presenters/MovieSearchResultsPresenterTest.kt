package com.joelglanfield.moviebite.presenters

import com.joelglanfield.moviebite.api.RemoteMovieProvider
import com.joelglanfield.moviebite.coroutines.TestContextProvider
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Test

class MovieSearchResultsPresenterTest {
    // mocks
    private val movieProvider: RemoteMovieProvider = mock()
    private val view: MovieSearchResultsPresenter.MovieListView = mock()

    // the search results presenter
    private lateinit var presenter: MovieSearchResultsPresenter

    @Before
    fun setup() {
        presenter = MovieSearchResultsPresenter(movieProvider, TestContextProvider().main)
    }

    /**
     * When searching a non-empty query, the view's updateMovieList() should be called precisely once.
     */
    @Test
    fun searchMovies_nonEmptyQuery_callsUpdateMovieListUI() {
        presenter.bind(view)

        runBlocking {
            presenter.searchMovies("star wars")

            // test with an empty list because we are relying on an API call for true results
            verify(view, times(1)).updateMovieListUI(emptyList())
        }
    }

    /**
     * When searching an empty query, the view's updateMovieList() should never be called.
     */
    @Test
    fun searchMovies_emptyQuery_neverCallsUpdateMovieListUI() {
        presenter.bind(view)

        runBlocking {
            presenter.searchMovies("")

            // test with an empty list because we are relying on an API call for true results
            verify(view, never()).updateMovieListUI(emptyList())
        }
    }

    /**
     * When calling load more, the view's updateMovieList() should be called precisely once.
     */
    @Test
    fun loadMore_callsUpdateMovieListUI() {
        presenter.bind(view)

        runBlocking {
            presenter.loadMore()

            // test with an empty list because we are relying on an API call for true results
            verify(view, times(1)).updateMovieListUI(emptyList())
        }

    }
}