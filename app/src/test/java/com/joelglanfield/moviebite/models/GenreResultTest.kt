package com.joelglanfield.moviebite.models

import org.junit.Assert.*
import org.junit.Test

/**
 * This class contains tests that cover all properties of the GenreResult model.
 */
class GenreResultTest {

    @Test
    fun testId() {
        val expectedId = 1
        val genreResult = GenreResult(expectedId, "")
        val actualId = genreResult.id

        assertEquals(expectedId, actualId)
    }

    @Test
    fun testName() {
        val expectedName = "name"
        val genreResult = GenreResult(1, expectedName)
        val actualName = genreResult.name

        assertEquals(expectedName, actualName)
    }
}