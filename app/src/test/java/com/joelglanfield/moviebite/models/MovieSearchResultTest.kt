package com.joelglanfield.moviebite.models

import org.junit.Assert.*
import org.junit.Test

/**
 * This class contains tests that cover all properties of the MovieSearchResult model.
 */
class MovieSearchResultTest {

    @Test
    fun testId() {
        val expectedId = 1L
        val movieSearchResult = MovieSearchResult(expectedId, "", "", 0.0, 0.0, "", emptyList())
        val actualId = movieSearchResult.id

        assertEquals(expectedId, actualId)
    }

    @Test
    fun testTitle() {
        val expectedTitle = "title"
        val movieSearchResult = MovieSearchResult(-1L, expectedTitle, "", 0.0, 0.0, "", emptyList())
        val actualTitle = movieSearchResult.title

        assertEquals(expectedTitle, actualTitle)
    }

    @Test
    fun testPosterPath() {
        val expectedPosterPath = "poster_path"
        val movieSearchResult = MovieSearchResult(-1L, "", expectedPosterPath, 0.0, 0.0, "", emptyList())
        val actualPosterPath = movieSearchResult.posterPath

        assertEquals(expectedPosterPath, actualPosterPath)
    }

    @Test
    fun testPopularity() {
        val expectedPopularity = 1.0
        val movieSearchResult = MovieSearchResult(-1L, "", "", expectedPopularity, 0.0, "", emptyList())
        val actualPopularity = movieSearchResult.popularity

        assertEquals(expectedPopularity, actualPopularity, 0.01)
    }

    @Test
    fun testVoteAverage() {
        val expectedVoteAverage = 1.0
        val movieSearchResult = MovieSearchResult(-1L, "", "", 0.0, expectedVoteAverage, "", emptyList())
        val actualVoteAverage = movieSearchResult.voteAverage

        assertEquals(expectedVoteAverage, actualVoteAverage, 0.01)
    }

    @Test
    fun testReleaseDate() {
        val expectedReleaseDate = "release_date"
        val movieSearchResult = MovieSearchResult(-1L, "", "", 0.0, 0.0, expectedReleaseDate, emptyList())
        val actualReleaseDate = movieSearchResult.releaseDate

        assertEquals(expectedReleaseDate, actualReleaseDate)
    }

    @Test
    fun testGenreIds_empty() {
        val expectedGenreIds = emptyList<Int>()
        val movieSearchResult = MovieSearchResult(-1L, "", "", 0.0, 0.0, "", expectedGenreIds)
        val actualGenreIds = movieSearchResult.genreIds

        assertEquals(expectedGenreIds, actualGenreIds)
    }

    @Test
    fun testGenreIds_nonEmpty() {
        val expectedGenreIds = listOf(1,2,3)
        val movieSearchResult = MovieSearchResult(-1L, "", "", 0.0, 0.0, "", expectedGenreIds)
        val actualGenreIds = movieSearchResult.genreIds

        assertEquals(expectedGenreIds, actualGenreIds)
    }
}