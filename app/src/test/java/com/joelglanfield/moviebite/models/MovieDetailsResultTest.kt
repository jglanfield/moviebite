package com.joelglanfield.moviebite.models

import org.junit.Assert.*
import org.junit.Test

/**
 * This class contains tests that cover all properties of the MovieDetailsResult model.
 */
class MovieDetailsResultTest {

    @Test
    fun testId() {
        val expectedId = 1L
        val movieDetailsResults = MovieDetailsResult(expectedId, "", "", "", "", 0.0, 0.0, "", emptyList())
        val actualId = movieDetailsResults.id

        assertEquals(expectedId, actualId)
    }

    @Test
    fun testTitle() {
        val expectedTitle = "title"
        val movieDetailsResults = MovieDetailsResult(-1L, expectedTitle, "", "", "", 0.0, 0.0, "", emptyList())
        val actualTitle = movieDetailsResults.title

        assertEquals(expectedTitle, actualTitle)
    }

    @Test
    fun testPosterPath() {
        val expectedPosterPath = "poster_path"
        val movieDetailsResults = MovieDetailsResult(-1L, "", expectedPosterPath, "", "", 0.0, 0.0, "", emptyList())
        val actualPosterPath = movieDetailsResults.posterPath

        assertEquals(expectedPosterPath, actualPosterPath)
    }

    @Test
    fun testBackdropPath() {
        val expectedBackdropPath = "backdrop_path"
        val movieDetailsResults = MovieDetailsResult(-1L, "", "", expectedBackdropPath, "", 0.0, 0.0, "", emptyList())
        val actualBackdropPath = movieDetailsResults.backdropPath

        assertEquals(expectedBackdropPath, actualBackdropPath)
    }

    @Test
    fun testOverview() {
        val expectedOverview = "overview"
        val movieDetailsResults = MovieDetailsResult(-1L, "", "", "", expectedOverview, 0.0, 0.0, "", emptyList())
        val actualOverview = movieDetailsResults.overview

        assertEquals(expectedOverview, actualOverview)
    }

    @Test
    fun testPopularity() {
        val expectedPopularity = 1.0
        val movieDetailsResults = MovieDetailsResult(-1L, "", "", "", "", expectedPopularity, 0.0, "", emptyList())
        val actualPopularity = movieDetailsResults.popularity

        assertEquals(expectedPopularity, actualPopularity, 0.01)
    }

    @Test
    fun testVoteAverage() {
        val expectedVoteAverage = 1.0
        val movieDetailsResults = MovieDetailsResult(-1L, "", "", "", "", 0.0, expectedVoteAverage, "", emptyList())
        val actualVoteAverage = movieDetailsResults.voteAverage

        assertEquals(expectedVoteAverage, actualVoteAverage, 0.01)
    }

    @Test
    fun testReleaseDate() {
        val expectedReleaseDate = "release_date"
        val movieDetailsResults = MovieDetailsResult(-1L, "", "", "", "", 0.0, 0.0, expectedReleaseDate, emptyList())
        val actualReleaseDate = movieDetailsResults.releaseDate

        assertEquals(expectedReleaseDate, actualReleaseDate)
    }

    @Test
    fun testGenres_empty() {
        val expectedGenreResults = emptyList<GenreResult>()
        val movieSearchResult = MovieDetailsResult(-1L, "", "", "", "", 0.0, 0.0, "", expectedGenreResults)
        val actualGenreResults = movieSearchResult.genres

        assertEquals(expectedGenreResults, actualGenreResults)
    }

    @Test
    fun testGenres_nonEmpty() {
        val expectedGenreResults = listOf(GenreResult(1, "name"))
        val movieSearchResult = MovieDetailsResult(-1L, "", "", "", "", 0.0, 0.0, "", expectedGenreResults)
        val actualGenreResults = movieSearchResult.genres

        assertEquals(expectedGenreResults, actualGenreResults)
    }
}