package com.joelglanfield.moviebite.models

import org.junit.Assert.*
import org.junit.Test

/**
 * This class contains tests that cover all properties of the GenreListResult model.
 */
class GenreListResultTest {

    @Test
    fun testGenres_empty() {
        val expectedGenres = emptyList<GenreResult>()
        val genreListResult = GenreListResult(expectedGenres)
        val actualGenres = genreListResult.genres

        assertEquals(expectedGenres, actualGenres)
    }

    @Test
    fun testGenres_nonEmpty() {
        val expectedGenres = listOf(GenreResult(1, "name"))
        val genreListResult = GenreListResult(expectedGenres)
        val actualGenres = genreListResult.genres

        assertEquals(expectedGenres, actualGenres)
    }
}