package com.joelglanfield.moviebite.models

import org.junit.Assert.*
import org.junit.Test

/**
 * This class contains tests that cover all properties of the MovieListResult model.
 */
class MovieListResultTest {

    @Test
    fun testPage() {
        val expectedPage = 10
        val movieListResult = MovieListResult(expectedPage, -1, -1, emptyList())
        val actualPage = movieListResult.page

        assertEquals(expectedPage, actualPage)
    }

    @Test
    fun testTotalPages() {
        val expectedTotalPages = 10
        val movieListResult = MovieListResult(-1, expectedTotalPages, -1, emptyList())
        val actualTotalPages = movieListResult.totalPages

        assertEquals(expectedTotalPages, actualTotalPages)
    }

    @Test
    fun testTotalResults() {
        val expectedTotalResults = 10
        val movieListResult = MovieListResult(-1, -1, expectedTotalResults, emptyList())
        val actualTotalResults = movieListResult.totalResults

        assertEquals(expectedTotalResults, actualTotalResults)
    }

    @Test
    fun testResults_empty() {
        val expectedResults = emptyList<MovieSearchResult>()
        val movieListResult = MovieListResult(-1, -1, -1, expectedResults)
        val actualResults = movieListResult.results

        assertEquals(expectedResults, actualResults)
    }

    @Test
    fun testResults_nonEmpty() {
        val movieSearchResult = MovieSearchResult(0, "title", "poster_path", 0.0, 0.0, "release_date", emptyList())
        val expectedResults = listOf(movieSearchResult)
        val movieListResult = MovieListResult(-1, -1, -1, expectedResults)
        val actualResults = movieListResult.results

        assertEquals(expectedResults, actualResults)
    }

}