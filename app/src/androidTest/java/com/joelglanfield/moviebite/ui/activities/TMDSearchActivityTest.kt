package com.joelglanfield.moviebite.ui.activities

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.widget.EditText
import com.joelglanfield.moviebite.R
import com.joelglanfield.moviebite.models.MovieSearchResult
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TMDSearchActivityTest {

    val rule = IntentsTestRule(TMDSearchActivity::class.java)
        @Rule get

    @Test
    fun testRecyclerViewIsConfigured() {
        val searchActivity = rule.activity
        val recyclerView = searchActivity.findViewById<RecyclerView>(R.id.moviesRecyclerView)

        assertNotNull(recyclerView)
        assertNotNull(recyclerView.adapter)
    }

    @Test
    fun testSearchQueryEntered() {
        val searchActivity = rule.activity
        val searchEditText = searchActivity.findViewById<EditText>(R.id.searchEditText)

        assertNotNull(searchEditText)

        val searchQuery = "solo a star wars story"

        Espresso.onView(ViewMatchers.withId(R.id.searchEditText))
                .perform(ViewActions.typeText(searchQuery))

        assertEquals(searchEditText.text.toString(), searchQuery)
    }

    @Test
    fun testSearchResultClicked_MovieDetailsActivityLaunched() {
        val searchActivity = rule.activity

        val movieSearchResult = MovieSearchResult(348350, "Solo: A Star Wars Story", "/4oD6VEccFkorEBTEDXtpLAaz0Rl.jpg", 7.6, 7.5, "2018-05-23", listOf(28,12,878))
        val movieResults = arrayListOf(movieSearchResult)

        searchActivity.moviesPresenter.movieResults = movieResults
        searchActivity.updateMovieListUI(movieResults)

        Espresso.onView(ViewMatchers.withId(R.id.moviesRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ViewActions.click()))


        Intents.intended(IntentMatchers.hasComponent(MovieDetailsActivity::class.java.name))
    }
}